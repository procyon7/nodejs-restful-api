const express = require("express");
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require("mongoose");

mongoose.connect(
    'mongodb://node-rest-shop:' +
    process.env.MONGO_ATLAS_PW +
    '@node-rest-shop-shard-00-00-7rcbq.mongodb.net:27017,node-rest-shop-shard-00-01-7rcbq.mongodb.net:27017,node-rest-shop-shard-00-02-7rcbq.mongodb.net:27017/test?ssl=true&replicaSet=node-rest-shop-shard-0&authSource=admin&retryWrites=true',
    { useNewUrlParser: true }
    );

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*'); //ne zaman bir response gönderildiğinde bu header'a sahip olacak. CORS'u önlemek için
    // *: to give access any origin
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization'

    );
    if(req.method === 'OPTIONS'){
        res.header("Access-Control-Allow-Methods","PUT,POST,PATCH,DELETE,GET");
        return res.status(200).json({});
    }
    // bir sonraki app use a geçmesi için yazman lazım
    next();
});

const productRoutes = require("./api/routes/products");  //importing routes
const orderRoutes = require("./api/routes/orders");
const userRoutes = require("./api/routes/users");

app.use(morgan('dev'));
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({extended: false})); //extended: false means that it supports simple bodies for url encoded data
app.use(bodyParser.json());  //extract json data and makes it easily readable to us

// server ile aynı adresten request gelirse hatasını engelliyor sanırsam
//routes which should handle request
app.use('/products', productRoutes);  //assigning routes
app.use('/orders', orderRoutes);   //assigning routes
app.use('/users', userRoutes);   //assigning routes

app.use((req, res, next) => {
   const error = new Error('Not found');
   error.status = 404;
   next(error);
});

app.use((error, req, res, next) =>{
    res.status(error.status || 500);
    res.json({
       error:{
           message: error.message
       }
    });
});


module.exports=app;
