const express = require('express');
const router = express.Router();
const mongoose = require("mongoose");
const multer = require("multer"); //  form-data parse etmeyi sağlıyor resimleri parse edip alabilmek için
const checkAuth = require('../middleware/check-auth');

const storage = multer.diskStorage({ // how to files are stored
    destination: function(req, file, cb){ // where to file to be stored
        cb(null, './uploads/');
    },
    filename: function(req, file, cb){ // how to name file and cb means callback
        cb(null, new Date().toISOString() + file.originalname); //tarih ve orijinal ismiyle kaydetme
    }
});

const fileFilter = function (req,file,cb){
    if(file.mimetype==="image/jpeg"||file.mimetype==="image/png"){ // image dosyasının uzantısını kıyaslama
        cb(null,true); // resmi kaydet demek
    }
    else{
        cb(null,false); //resmi ignore et demek
    }
}

const upload = multer({
    storage:storage,
    limits:{ // limitleri belirlemek için bite cinsinden
        fileSize : 1024 * 1024 *5
    },
    fileFilter: fileFilter
}); // initiliaze edip onları depolayacağımız lokasyonu veriyoruz

const Product = require('../models/product');

router.get('/', (req, res, next) => {
    Product.find()
        .select("name price _id productImage") //göndermek istediğin fieldları seçiyorsun
        .exec()
        .then(docs=>{
            const response = { //yeni bir var yaratıp sayısını ve productaları gönderiyorsun
                count: docs.length,
                products: docs.map(doc=>{
                    return {
                        name: doc.name,
                        price: doc.price,
                        productImage: doc.productImage,
                        _id: doc._id,
                        request: {
                            type:"GET",
                            url:"http://localhost:3000/products/" + doc._id
                        }
                    }
                })
            };
            res.status(200).json(response);
        })
        .catch( err => {
            console.log(err);
            res.status(500).json({error : err});
        });
});

router.post('/', checkAuth ,upload.single('productImage'), (req, res, next) => {
    const product = new Product({
        _id : new mongoose.Types.ObjectId(),
        name : req.body.name,
        price : req.body.price,
        productImage : req.file.path
    });
    //saves product into the db
    product
        .save()
        .then(result => {
        console.log(result);
        res.status(201).json({
            message: 'Product created successfully!',
            createdProduct: {
                name: result.name,
                price: result.price,
                productImage : result.productImage,
                _id: result._id,
                request: {
                    type: "GET",
                    url : "http://localhost:3000/products/"+ result._id
                }
            }
        });
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });


});

router.get('/:productId', (req, res, next) => {
    const id = req.params.productId;

    Product.findById(id)
        .select("name price _id productImage")
        .exec()
        .then(doc=>{
            console.log("From db", doc);
            if(doc){
                res.status(200).json({
                    product:doc,
                    request : {
                        type : "GET",
                        url : "http://localhost:3000/products"
                    }
                });
            }
            else{
                res.status(404).json({message:"No valid entry found for provided Id"});
            }


        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({error:err});
        });

});

router.patch("/:productID",(req,res,next)=>{
    const id = req.params.productID;

    // değişecek propertyleri tutacak variable
    const updateOps = {};

    //değişecek variableları requestten alıp değiştiriyor
    for(const ops of req.body){
        updateOps[ops.propName] = ops.value;
    }
    // ilk kısım product ı buluyor ikinci kısım nasıl update edileceğini gösteriyor
    Product.update({_id: id},{$set: updateOps})
        .exec()
        .then(result=>{
            res.status(200).json({
                message : " Product updated!",
                request:{
                    type: "GET",
                    url : "http://localhost:3000/products/" + id
                }
            });
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({error:err});
        });

});

// productlaru silmek için kullanılıyor

router.delete("/:productID",(req,res,next)=>{
    //id yi alıyoruz
    const id=req.params.productID;
    //bu bir fonksiyon remove u execute et then resultı status code 200 ile gönder ya da err catch et status code 500 ile gönder
    Product.remove({_id:id})
        .exec()
        .then(result=>{
            res.status(200).json({
                message : "Product deleted!",
                request:{
                    type : "POST",
                    url : "http://localhost:3000/products/",
                    body :{name:"String",price:"Number"}
                }
            });
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({error:err});
        });
});

module.exports = router;